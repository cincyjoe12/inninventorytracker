﻿using System.Collections.Generic;
using InnInventoryTracker.RemoteClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InnInventoryTracker.Tests
{
    [TestClass]
    public class UnitTests
    {
        private readonly OriginalCode _originalCode;
        private readonly NewCode _newCode;

        public UnitTests()
        {
            _originalCode = new OriginalCode();
            _newCode = new NewCode();
        }

        [TestMethod]
        public void AfterDay1Test()
        {
            _originalCode.UpdateQuality();
            _newCode.UpdateQuality();

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(9, list[0].SellIn);
                Assert.AreEqual(19, list[0].Quality);
                Assert.AreEqual(1, list[1].SellIn);
                Assert.AreEqual(1, list[1].Quality);
                Assert.AreEqual(4, list[2].SellIn);
                Assert.AreEqual(6, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(14, list[4].SellIn);
                Assert.AreEqual(21, list[4].Quality);
                Assert.AreEqual(2, list[5].SellIn);
                Assert.AreEqual(5, list[5].Quality);
            }

        }

        [TestMethod]
        public void AfterDay2Test()
        {
            for (int i = 0; i < 2; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(8, list[0].SellIn);
                Assert.AreEqual(18, list[0].Quality);
                Assert.AreEqual(0, list[1].SellIn);
                Assert.AreEqual(2, list[1].Quality);
                Assert.AreEqual(3, list[2].SellIn);
                Assert.AreEqual(5, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(13, list[4].SellIn);
                Assert.AreEqual(22, list[4].Quality);
                Assert.AreEqual(1, list[5].SellIn);
                Assert.AreEqual(4, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay3Test()
        {
            for (int i = 0; i < 3; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(7, list[0].SellIn);
                Assert.AreEqual(17, list[0].Quality);
                Assert.AreEqual(-1, list[1].SellIn);
                Assert.AreEqual(4, list[1].Quality);
                Assert.AreEqual(2, list[2].SellIn);
                Assert.AreEqual(4, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(12, list[4].SellIn);
                Assert.AreEqual(23, list[4].Quality);
                Assert.AreEqual(0, list[5].SellIn);
                Assert.AreEqual(3, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay4Test()
        {
            for (int i = 0; i < 4; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(6, list[0].SellIn);
                Assert.AreEqual(16, list[0].Quality);
                Assert.AreEqual(-2, list[1].SellIn);
                Assert.AreEqual(6, list[1].Quality);
                Assert.AreEqual(1, list[2].SellIn);
                Assert.AreEqual(3, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(11, list[4].SellIn);
                Assert.AreEqual(24, list[4].Quality);
                Assert.AreEqual(-1, list[5].SellIn);
                Assert.AreEqual(1, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay5Test()
        {
            for (int i = 0; i < 5; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(5, list[0].SellIn);
                Assert.AreEqual(15, list[0].Quality);
                Assert.AreEqual(-3, list[1].SellIn);
                Assert.AreEqual(8, list[1].Quality);
                Assert.AreEqual(0, list[2].SellIn);
                Assert.AreEqual(2, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(10, list[4].SellIn);
                Assert.AreEqual(25, list[4].Quality);
                Assert.AreEqual(-2, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay6Test()
        {
            for (int i = 0; i < 6; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(4, list[0].SellIn);
                Assert.AreEqual(14, list[0].Quality);
                Assert.AreEqual(-4, list[1].SellIn);
                Assert.AreEqual(10, list[1].Quality);
                Assert.AreEqual(-1, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(9, list[4].SellIn);
                Assert.AreEqual(27, list[4].Quality);
                Assert.AreEqual(-3, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay7Test()
        {
            for (int i = 0; i < 7; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(3, list[0].SellIn);
                Assert.AreEqual(13, list[0].Quality);
                Assert.AreEqual(-5, list[1].SellIn);
                Assert.AreEqual(12, list[1].Quality);
                Assert.AreEqual(-2, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(8, list[4].SellIn);
                Assert.AreEqual(29, list[4].Quality);
                Assert.AreEqual(-4, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay8Test()
        {
            for (int i = 0; i < 8; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(2, list[0].SellIn);
                Assert.AreEqual(12, list[0].Quality);
                Assert.AreEqual(-6, list[1].SellIn);
                Assert.AreEqual(14, list[1].Quality);
                Assert.AreEqual(-3, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(7, list[4].SellIn);
                Assert.AreEqual(31, list[4].Quality);
                Assert.AreEqual(-5, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay9Test()
        {
            for (int i = 0; i < 9; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(1, list[0].SellIn);
                Assert.AreEqual(11, list[0].Quality);
                Assert.AreEqual(-7, list[1].SellIn);
                Assert.AreEqual(16, list[1].Quality);
                Assert.AreEqual(-4, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(6, list[4].SellIn);
                Assert.AreEqual(33, list[4].Quality);
                Assert.AreEqual(-6, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay10Test()
        {
            for (int i = 0; i < 10; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(0, list[0].SellIn);
                Assert.AreEqual(10, list[0].Quality);
                Assert.AreEqual(-8, list[1].SellIn);
                Assert.AreEqual(18, list[1].Quality);
                Assert.AreEqual(-5, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(5, list[4].SellIn);
                Assert.AreEqual(35, list[4].Quality);
                Assert.AreEqual(-7, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay11Test()
        {
            for (int i = 0; i < 11; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-1, list[0].SellIn);
                Assert.AreEqual(8, list[0].Quality);
                Assert.AreEqual(-9, list[1].SellIn);
                Assert.AreEqual(20, list[1].Quality);
                Assert.AreEqual(-6, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(4, list[4].SellIn);
                Assert.AreEqual(38, list[4].Quality);
                Assert.AreEqual(-8, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay12Test()
        {
            for (int i = 0; i < 12; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-2, list[0].SellIn);
                Assert.AreEqual(6, list[0].Quality);
                Assert.AreEqual(-10, list[1].SellIn);
                Assert.AreEqual(22, list[1].Quality);
                Assert.AreEqual(-7, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(3, list[4].SellIn);
                Assert.AreEqual(41, list[4].Quality);
                Assert.AreEqual(-9, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay13Test()
        {
            for (int i = 0; i < 13; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-3, list[0].SellIn);
                Assert.AreEqual(4, list[0].Quality);
                Assert.AreEqual(-11, list[1].SellIn);
                Assert.AreEqual(24, list[1].Quality);
                Assert.AreEqual(-8, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(2, list[4].SellIn);
                Assert.AreEqual(44, list[4].Quality);
                Assert.AreEqual(-10, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay14Test()
        {
            for (int i = 0; i < 14; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-4, list[0].SellIn);
                Assert.AreEqual(2, list[0].Quality);
                Assert.AreEqual(-12, list[1].SellIn);
                Assert.AreEqual(26, list[1].Quality);
                Assert.AreEqual(-9, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(1, list[4].SellIn);
                Assert.AreEqual(47, list[4].Quality);
                Assert.AreEqual(-11, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay15Test()
        {
            for (int i = 0; i < 15; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-5, list[0].SellIn);
                Assert.AreEqual(0, list[0].Quality);
                Assert.AreEqual(-13, list[1].SellIn);
                Assert.AreEqual(28, list[1].Quality);
                Assert.AreEqual(-10, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(0, list[4].SellIn);
                Assert.AreEqual(50, list[4].Quality);
                Assert.AreEqual(-12, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay16Test()
        {
            for (int i = 0; i < 16; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-6, list[0].SellIn);
                Assert.AreEqual(0, list[0].Quality);
                Assert.AreEqual(-14, list[1].SellIn);
                Assert.AreEqual(30, list[1].Quality);
                Assert.AreEqual(-11, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(-1, list[4].SellIn);
                Assert.AreEqual(0, list[4].Quality);
                Assert.AreEqual(-13, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay26Test()
        {
            for (int i = 0; i < 26; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-16, list[0].SellIn);
                Assert.AreEqual(0, list[0].Quality);
                Assert.AreEqual(-24, list[1].SellIn);
                Assert.AreEqual(50, list[1].Quality);
                Assert.AreEqual(-21, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(-11, list[4].SellIn);
                Assert.AreEqual(0, list[4].Quality);
                Assert.AreEqual(-23, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }

        [TestMethod]
        public void AfterDay27Test()
        {
            for (int i = 0; i < 27; i++)
            {
                _originalCode.UpdateQuality();
                _newCode.UpdateQuality();
            }

            List<IList<Item>> listOfLists = new List<IList<Item>>();
            listOfLists.Add(_originalCode.Items);
            listOfLists.Add(_newCode.Items);

            foreach (var list in listOfLists)
            {
                Assert.AreEqual(-17, list[0].SellIn);
                Assert.AreEqual(0, list[0].Quality);
                Assert.AreEqual(-25, list[1].SellIn);
                Assert.AreEqual(50, list[1].Quality);
                Assert.AreEqual(-22, list[2].SellIn);
                Assert.AreEqual(0, list[2].Quality);
                Assert.AreEqual(0, list[3].SellIn);
                Assert.AreEqual(80, list[3].Quality);
                Assert.AreEqual(-12, list[4].SellIn);
                Assert.AreEqual(0, list[4].Quality);
                Assert.AreEqual(-24, list[5].SellIn);
                Assert.AreEqual(0, list[5].Quality);
            }
        }
    }
}
