﻿using InnInventoryTracker.RemoteClasses;

namespace InnInventoryTracker.Classes
{
    public class ConjuredItem : NormalItem
    {
        private const int QUALITY_DECAY_FACTOR = 2;

        public ConjuredItem(Item item) : base(item)
        {
        }

        public void Update()
        {
            base.Update(QUALITY_DECAY_FACTOR);
        }
    }
}
