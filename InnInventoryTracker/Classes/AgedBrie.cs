﻿using InnInventoryTracker.RemoteClasses;

namespace InnInventoryTracker.Classes
{
    public class AgedBrie : NormalItem
    {
        public AgedBrie(Item item) : base(item)
        {
        }

        public new void Update()
        {
            IncreaseQualityBy(IsExpired() ? 2 : 1);
        }
    }
}
