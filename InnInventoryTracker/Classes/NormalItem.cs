﻿using InnInventoryTracker.RemoteClasses;

namespace InnInventoryTracker.Classes
{
    public class NormalItem
    {
        private const int MIN_QUALITY = 0;
        private const int MAX_QUALITY = 50;

        protected readonly Item Item;
        public NormalItem(Item item)
        {
            Item = item;
        }

        public void Update(int qualityDecayFactor = 1)
        {
            DecreaseQualityBy(qualityDecayFactor * (IsExpired() ? 2 : 1));
        }

        protected bool IsExpired()
        {
            return Item.SellIn < 0;
        }

        protected void DecreaseQualityBy(int numberToSubtract)
        {
            Item.Quality -= numberToSubtract;
            ConstrainQualityToNoLessThan(MIN_QUALITY);
        }

        protected void IncreaseQualityBy(int numberToAdd)
        {
            Item.Quality += numberToAdd;
            ConstrainQualityToNoMoreThan(MAX_QUALITY);
        }

        private void ConstrainQualityToNoMoreThan(int maxValue)
        {
            if (Item.Quality > maxValue)
                Item.Quality = maxValue;
        }

        private void ConstrainQualityToNoLessThan(int minValue)
        {
            if (Item.Quality < minValue)
                Item.Quality = minValue;
        }
    }
}
