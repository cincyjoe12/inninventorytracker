﻿using InnInventoryTracker.RemoteClasses;

namespace InnInventoryTracker.Classes
{

    public class BackstagePass : NormalItem
    {
        private const int FIVE_DAYS = 5;
        private const int TEN_DAYS = 10;

        public BackstagePass(Item item) : base(item)
        {
        }

        public new void Update()
        {
            if (IsExpired())
                DecreaseQualityBy(Item.Quality);
            else
            {
                IncreaseQualityBy(1);
                if (Item.SellIn < TEN_DAYS)
                    IncreaseQualityBy(1);
                if (Item.SellIn < FIVE_DAYS)
                    IncreaseQualityBy(1);
            }
        }
    }
}
