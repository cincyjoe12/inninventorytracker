﻿using System;
using System.Collections.Generic;
using InnInventoryTracker.Classes;
using InnInventoryTracker.RemoteClasses;

namespace InnInventoryTracker
{
    public class NewCode
    {
        private const string SULFURAS = "Sulfuras, Hand of Ragnaros";
        private const string AGED_BRIE = "Aged Brie";
        private const string BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
        private const string CONJURED = "CONJURED";


        public IList<Item> Items = new List<Item>
        {
            new Item { Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20 },
            new Item { Name = "Aged Brie", SellIn = 2, Quality = 0 },
            new Item { Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7 },
            new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 },
            new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 15, Quality = 20 },
            new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 6 }
        };

        public void Execute()
        {
            int counter = 0;
            bool nextDayRequested = true;
            DisplayInventory(ref counter);
            Console.WriteLine("\nPress Enter to go to next day. Type 'exit' to end program.");
            string response = Console.ReadLine();
            if (response == "exit")
                nextDayRequested = false;

            while (nextDayRequested)
            {
                UpdateQuality();
                DisplayInventory(ref counter);

                Console.WriteLine("\nPress Enter to go to next day. Type 'exit' to end program.");
                response = Console.ReadLine();
                if (response == "exit")
                    nextDayRequested = false;
            }

        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                if (Items[i].Name == SULFURAS)
                    continue;

                Items[i].SellIn -= 1;
                switch (Items[i].Name)
                {
                    case AGED_BRIE:
                        AgedBrie agedBrie = new AgedBrie(Items[i]);   
                        agedBrie.Update();
                        break;
                    case BACKSTAGE_PASSES:
                        BackstagePass backstagePass = new BackstagePass(Items[i]);
                        backstagePass.Update();
                        break;
                    case CONJURED:
                        ConjuredItem conjuredItem = new ConjuredItem(Items[i]);
                        conjuredItem.Update();
                        break;
                    default:
                        NormalItem standardItem = new NormalItem(Items[i]);
                        standardItem.Update();
                        break;
                }
            }
        }

        private void DisplayInventory(ref int counter)
        {
            Console.WriteLine("Day " + counter);
            Console.WriteLine("Name".PadRight(50, ' ') + "SellIn".PadRight(7, ' ') + "Quality");
            for (var i = 0; i < Items.Count; i++)
            {
                Console.WriteLine(Items[i].Name.PadRight(50, ' ') + "  " + Items[i].SellIn.ToString().PadRight(7, ' ') + "  " + Items[i].Quality.ToString().PadRight(2, ' '));
            }
            counter++;
        }
    }
}
